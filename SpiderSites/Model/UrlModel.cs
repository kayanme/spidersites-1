﻿using System;

namespace SpiderSites.Model
{
    public class UrlModel : IEquatable<UrlModel>
    {
        /// <summary>
        /// Ссылка
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Состояние работы
        /// </summary>
        public UrlStateType State { get; set; }

        #region Common

        /// <summary>
        /// Типы состояния работы
        /// </summary>
        public enum UrlStateType
        {
            None,

            /// <summary>
            /// В процессе парсинга
            /// </summary>
            Parse,

            /// <summary>
            /// Парсинг завершен
            /// </summary>
            Complete,

            /// <summary>
            /// Иная ссылка
            /// </summary>
            OtherLink,
        }

        #endregion Common

        #region Re

        public bool Equals(UrlModel other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Url == other.Url;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((UrlModel)obj);
        }

        public override int GetHashCode()
        {
            if (string.IsNullOrEmpty(Url))
                return 0;

            var hashCode = Url.GetHashCode();
            return hashCode;
        }

        #endregion Re
    }
}