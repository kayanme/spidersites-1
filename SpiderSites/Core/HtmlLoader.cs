﻿using SpiderSites.Model;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace SpiderSites.Core
{
    public class HtmlLoader
    {
        private readonly string _url;

        public HtmlLoader(IParserSetting setting)
        {
            _url = $"{(setting.UseHttps ? "http" : "https")}://{setting.BaseUrl}";
        }

        public async Task<ResponseModel<string>> GetSourcePage(string urlPage)
        {
            var result = new ResponseModel<string> { IsOk = false };

            var request = WebRequest.Create(string.Concat(_url, urlPage));
            request.Credentials = CredentialCache.DefaultCredentials;

            try
            {
                var response = request.GetResponse();
                result.Code = ((HttpWebResponse)response).StatusCode;

                using (var dataStream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(dataStream ?? throw new InvalidOperationException()))
                    {
                        result.Data = await reader.ReadToEndAsync();
                    }
                }

                response.Close();
                result.IsOk = true;
            }
            catch (Exception e)
            {
                result.Error = e.Message;
            }

            return result;
        }
    }
}